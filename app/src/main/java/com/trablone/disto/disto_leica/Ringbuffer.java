package com.trablone.disto.disto_leica;

public class Ringbuffer {
    private int f5N = 0;
    private byte[] f6a;
    private int first = 0;
    private int last = 0;

    public Ringbuffer(int capacity) {
        this.f6a = new byte[capacity];
    }

    public boolean isEmpty() {
        return this.f5N == 0;
    }

    public int size() {
        return this.f5N;
    }

    public void enqueue(byte b) {
        if (this.f5N == this.f6a.length) {
            throw new RuntimeException("Ring buffer overflow");
        }
        this.f6a[this.last] = b;
        this.last = (this.last + 1) % this.f6a.length;
        this.f5N++;
    }

    public byte dequeue() {
        if (isEmpty()) {
            throw new RuntimeException("Ring buffer underflow");
        }
        byte item = this.f6a[this.first];
        this.f5N--;
        this.first = (this.first + 1) % this.f6a.length;
        return item;
    }

    public byte peek() {
        if (isEmpty()) {
            return (byte) 0;
        }
        return this.f6a[this.first];
    }

    public boolean contains(byte b) {
        if (isEmpty()) {
            return false;
        }
        int n = this.first;
        while (n != this.last) {
            if (this.f6a[n] == b) {
                return true;
            }
            n = (n + 1) % this.f6a.length;
        }
        return false;
    }

    public void insert(byte[] line, int n) {
        for (int i = 0; i < n; i++) {
            enqueue(line[i]);
        }
    }

    public int getLine(byte[] line, byte eolChar) {
        int nMax = line.length;
        int n = 0;
        while (n < nMax) {
            try {
                line[n] = dequeue();
                if (line[n] == eolChar) {
                    break;
                }
                n++;
            } catch (RuntimeException e) {
            }
        }
        return n;
    }

    public int dequeueUntil(byte[] bytes) {
        boolean validBegin = false;
        int n = 0;
        do {
            try {
                byte ch = peek();
                n = 0;
                while (n < bytes.length) {
                    if (bytes[n] == ch) {
                        validBegin = true;
                        break;
                    }
                    n++;
                }
                if (!validBegin) {
                    dequeue();
                    continue;
                }
            } catch (RuntimeException e) {
            }
        } while (!validBegin);
        return n;
    }
}
