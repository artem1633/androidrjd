package com.trablone.disto.disto_leica;

public interface OnConnectionStateChanged {
    void onDeviceConnected(String str);

    void onDeviceDisconnected();
}
