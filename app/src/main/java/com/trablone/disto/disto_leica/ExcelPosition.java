package com.trablone.disto.disto_leica;

class ExcelPosition {
    private final int kMaxCols = 255;
    private final int kMaxRows = 65536;
    private int m_GridPosX = 1;
    private int m_GridPosY = 1;
    private int m_GridViewposition = 0;
    boolean m_bOffsetChanged = false;
    private int m_nExcelDataGridViewXOffset = 0;
    private int m_nExcelDataGridViewYOffset = 0;
    int m_nExcelDataSizeX = 0;
    int m_nExcelDataSizeY = 0;
    int m_nSizeGridViewX = 0;
    int m_nSizeGridViewY = 0;

    ExcelPosition() {
    }

    void clearExcelDataInfo() {
        this.m_nExcelDataGridViewYOffset = 0;
        this.m_nExcelDataGridViewXOffset = 0;
        this.m_nExcelDataSizeY = 0;
        this.m_nExcelDataSizeX = 0;
    }

    void setGridPos(int position) {
        this.m_GridViewposition = position;
        this.m_GridPosX = this.m_GridViewposition;
        if (this.m_nSizeGridViewY > 0) {
            this.m_GridPosX = position % this.m_nSizeGridViewX;
        }
        if (this.m_nSizeGridViewX > 0) {
            this.m_GridPosY = (position - this.m_GridPosX) / this.m_nSizeGridViewX;
        }
    }

    void setGridPosX(int x) {
        if (x >= 0) {
            if (x < this.m_nSizeGridViewX) {
                this.m_GridPosX = x;
                this.m_GridViewposition = (this.m_nSizeGridViewX * this.m_GridPosY) + this.m_GridPosX;
                return;
            }
            this.m_bOffsetChanged = true;
            this.m_nExcelDataGridViewXOffset += x - this.m_GridPosX;
            if (this.m_nExcelDataGridViewXOffset + this.m_nSizeGridViewX >= 255) {
                this.m_nExcelDataGridViewXOffset = 255 - this.m_nSizeGridViewX;
            }
        } else if (this.m_nExcelDataGridViewXOffset > 0) {
            this.m_bOffsetChanged = true;
            if (this.m_nExcelDataGridViewXOffset + x >= 0) {
                this.m_nExcelDataGridViewXOffset += x;
            } else {
                this.m_nExcelDataGridViewXOffset = 0;
            }
        }
    }

    void setGridPosY(int y) {
        if (y >= 0) {
            if (y < this.m_nSizeGridViewY) {
                this.m_GridPosY = y;
                this.m_GridViewposition = (this.m_nSizeGridViewX * this.m_GridPosY) + this.m_GridPosX;
                return;
            }
            this.m_bOffsetChanged = true;
            this.m_nExcelDataGridViewYOffset += y - this.m_GridPosY;
        } else if (this.m_nExcelDataGridViewYOffset > 0) {
            this.m_bOffsetChanged = true;
            if (this.m_nExcelDataGridViewYOffset + y >= 0) {
                this.m_nExcelDataGridViewYOffset += y;
            } else {
                this.m_nExcelDataGridViewYOffset = 0;
            }
        }
    }

    int getGridPosition() {
        return this.m_GridViewposition;
    }

    int getGridSize() {
        return this.m_nSizeGridViewX * this.m_nSizeGridViewY;
    }

    int getExcelPosX() {
        int ret = this.m_GridPosX + this.m_nExcelDataGridViewXOffset;
        if (ret < 0) {
            return 0;
        }
        return ret;
    }

    int getExcelPosY() {
        int ret = this.m_GridPosY + this.m_nExcelDataGridViewYOffset;
        if (ret < 0) {
            return 0;
        }
        return ret;
    }

    int getExcelViewXmin() {
        return this.m_nExcelDataGridViewXOffset;
    }

    int getExcelViewXmax() {
        return this.m_nExcelDataGridViewXOffset + this.m_nSizeGridViewX;
    }

    int getExcelViewYmin() {
        return this.m_nExcelDataGridViewYOffset;
    }

    int getExcelViewYmax() {
        return this.m_nExcelDataGridViewYOffset + this.m_nSizeGridViewY;
    }

    int getGridPosFromExcelXY(int x, int y) {
        return (this.m_nSizeGridViewX * (y - this.m_nExcelDataGridViewYOffset)) + (x - this.m_nExcelDataGridViewXOffset);
    }

    int setGridPosFromExcelXY() {
        this.m_GridViewposition = getGridPosFromExcelXY(this.m_GridPosX, this.m_GridPosY);
        return this.m_GridViewposition;
    }

    void up() {
        setGridPosY(this.m_GridPosY - 1);
    }

    void down() {
        setGridPosY(this.m_GridPosY + 1);
    }

    void left() {
        setGridPosX(this.m_GridPosX - 1);
    }

    void right() {
        setGridPosX(this.m_GridPosX + 1);
    }

    void scrollUp() {
        if (this.m_nExcelDataGridViewYOffset > 0) {
            this.m_nExcelDataGridViewYOffset--;
            this.m_bOffsetChanged = true;
            return;
        }
        this.m_nExcelDataGridViewYOffset = 0;
    }

    void scrollDown() {
        if (this.m_nExcelDataGridViewYOffset + this.m_nSizeGridViewY < 65536) {
            this.m_nExcelDataGridViewYOffset++;
            this.m_bOffsetChanged = true;
        }
    }

    void scrollRight() {
        if (this.m_nExcelDataGridViewXOffset + this.m_nSizeGridViewX < 255) {
            this.m_nExcelDataGridViewXOffset++;
            this.m_bOffsetChanged = true;
        }
    }

    void scrollLeft() {
        if (this.m_nExcelDataGridViewXOffset > 0) {
            this.m_nExcelDataGridViewXOffset--;
            this.m_bOffsetChanged = true;
            return;
        }
        this.m_nExcelDataGridViewXOffset = 0;
    }

    public String toString() {
        return String.format("Gridsize %d %d, PosXY %d %d, Pos %d, ExcelSizeXY %d %d, Offset %d %d, bOffsetChanged %b", new Object[]{Integer.valueOf(this.m_nSizeGridViewX), Integer.valueOf(this.m_nSizeGridViewY), Integer.valueOf(this.m_GridPosX), Integer.valueOf(this.m_GridPosY), Integer.valueOf(this.m_GridViewposition), Integer.valueOf(this.m_nExcelDataSizeX), Integer.valueOf(this.m_nExcelDataSizeY), Integer.valueOf(this.m_nExcelDataGridViewXOffset), Integer.valueOf(this.m_nExcelDataGridViewYOffset), Boolean.valueOf(this.m_bOffsetChanged)});
    }
}
