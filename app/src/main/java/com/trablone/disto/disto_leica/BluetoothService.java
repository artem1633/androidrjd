package com.trablone.disto.disto_leica;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.text.format.Time;
import android.util.Log;

import com.trablone.disto.MainActivity;
import com.trablone.disto.R;
import com.trablone.disto.models.Measure;

import org.greenrobot.eventbus.EventBus;

import java.util.UUID;

import io.realm.Realm;

import static com.trablone.disto.SettingsActivity.EDIT_Y;
import static com.trablone.disto.SettingsActivity.EDIT_Z;
import static com.trablone.disto.SettingsActivity.TYPE_Y_0_1_BEFORE;
import static com.trablone.disto.SettingsActivity.TYPE_Y_0_1_FROM;
import static com.trablone.disto.SettingsActivity.TYPE_Z_0_1_BEFORE;
import static com.trablone.disto.SettingsActivity.TYPE_Z_0_1_FROM;
import static com.trablone.disto.SettingsActivity.TYPE_Z_2_FROM;
import static com.trablone.disto.SettingsActivity.TYPE_Z_3_4_FROM;
import static com.trablone.disto.SettingsActivity.TYPE_Z_5_FROM;


public class BluetoothService extends Service {

    private static final String METER_UUID_CRACTER = "0000cbb1-0000-1000-8000-00805f9b34fb";
    private static final String METER_UUID_SERVICE = "0000cbbb-0000-1000-8000-00805f9b34fb";

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothManager bluetoothManager;

    private BluetoothGatt connectedDevice = null;
    private OnConnectionStateChanged connectionStateChanged;
    private BluetoothGattCallback callback = new C03951();
    private final IBinder binder = new BleBinder();
    private Handler handler = new Handler();
    private BluetoothGatt m_BluetoothGatt;
    private NotificationManagerCompat mNotificationManager;
    public static final String CHANNEL_ID = "rjd_channel_01";
    private long mNotificationPostTime = 0;
    private SharedPreferences preferences;
    private BleDevice device;

    public void setMeasureId(String measureId) {
        this.measureId = measureId;
    }

    public String getMeasureId() {
        return measureId;
    }

    private String measureId;

    public BluetoothService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        mNotificationManager = NotificationManagerCompat.from(this);
        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "RJD";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            manager.createNotificationChannel(mChannel);
        }
    }

    private Notification buildNotification(String text) {

        Intent nowPlayingIntent = new Intent(this, MainActivity.class);
        PendingIntent clickIntent = PendingIntent.getActivity(this, 0, nowPlayingIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (mNotificationPostTime == 0) {
            mNotificationPostTime = System.currentTimeMillis();
        }

        android.support.v4.app.NotificationCompat.Builder builder = new android.support.v4.app.NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(clickIntent)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(text)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(mNotificationPostTime);

        Notification n = builder.build();

        return n;
    }

    public class BleBinder extends Binder {
        public BluetoothService getService() {
            return BluetoothService.this;
        }
    }

    @Nullable
    public IBinder onBind(Intent intent) {
        return this.binder;
    }


    public int onStartCommand(Intent intent, int flags, int startId) {
        this.bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = this.bluetoothManager.getAdapter();

        if (device != null) {
            connectToDevice(device);
        }
        return Service.START_STICKY;
    }

    public BleDevice getDevice(){
        return device;
    }

    public class C03951 extends BluetoothGattCallback {

        BleDistoMeasurementFormat m_CurrentMeasure;
        String test;

        C03951() {
        }

        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

            //Log.e("tr", "state: " + status + " newState: " + newState);
            if (newState == BluetoothGatt.STATE_CONNECTED) {
                EventBus.getDefault().post(new BleDevice(gatt.getDevice()));
                mNotificationManager.notify(0, buildNotification("Подключен " + gatt.getDevice().getName()));
                //connectedDevice = gatt;
                gatt.discoverServices();
            } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                EventBus.getDefault().post(new BleDevice("Отключено " + gatt.getDevice().getName()));
                mNotificationManager.notify(0, buildNotification("Отключено " + gatt.getDevice().getName()));
                if (device != null) {
                    device.setDeviceName("Отключено " + gatt.getDevice().getName());
                    if (connectedDevice != null) {
                        connectedDevice.close();
                        connectedDevice.disconnect();
                        //this.connectionStateChanged.onDeviceDisconnected();
                        mNotificationManager.cancelAll();
                        connectedDevice = null;
                    }
                    connectToDevice(device);
                }
            } else if (newState == BluetoothGatt.STATE_CONNECTING) {
                EventBus.getDefault().post(new BleDevice("Подключение"));
            }
        }

        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
            gatt.setCharacteristicNotification(gatt.getService(UUID.fromString(BluetoothService.METER_UUID_SERVICE)).getCharacteristic(UUID.fromString(BluetoothService.METER_UUID_CRACTER)), true);
        }

        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

            String val = characteristic.getStringValue(0);
            //Log.e("tr", "val " + val + " " + characteristic.getStringValue(1) + this);
            try {
                val = getData(val);
                double distance = Double.parseDouble(val);

                if (m_CurrentMeasure == null) {
                    m_CurrentMeasure = new BleDistoMeasurementFormat();
                    this.m_CurrentMeasure.m_Time = new Time();
                    this.m_CurrentMeasure.m_Time.setToNow();
                    this.m_CurrentMeasure.m_dBleDistance = distance;
                } else {
                    this.m_CurrentMeasure.m_dBleAngle = distance;
                    if (measureId != null) {
                        updateMeasure(m_CurrentMeasure);
                    } else {
                        metricFromBle(m_CurrentMeasure);
                    }

                    m_CurrentMeasure = null;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                m_CurrentMeasure = null;
            }
        }
    }

    public void metricFromBle(final BleDistoMeasurementFormat aMeasObj) {

        final Realm realm = Realm.getDefaultInstance();
        writeMeasure(realm, aMeasObj, true);

    }

    private void updateMeasure(BleDistoMeasurementFormat aMeasObj) {
        Realm realm = Realm.getDefaultInstance();
        Measure measure = realm.where(Measure.class).equalTo("id", measureId).findFirst();
        measureId = null;
        realm.beginTransaction();
        int editY = preferences.getInt("editY", EDIT_Y);
        int editZ = preferences.getInt("editZ", EDIT_Z);

        int ca = (int) (aMeasObj.m_dBleDistance * 1000);

        measure.setY((int) (ca * Math.sin(Math.toRadians(aMeasObj.m_dBleAngle))) + editY);
        measure.setZ((int) (ca * Math.cos(Math.toRadians(aMeasObj.m_dBleAngle))) + editZ);
        measure.setAngle(aMeasObj.m_dBleAngle);
        measure.setDistance(ca);
        checkTolerance(measure);
        realm.commitTransaction();
    }

    public void writeMeasure(Realm realm, BleDistoMeasurementFormat aMeasObj, boolean calc) {
        realm.beginTransaction();

        Measure item = realm.createObject(Measure.class, UUID.randomUUID().toString());

        String type = preferences.getString("type_select", "Борт\nплатформы");
        int type_position = preferences.getInt("type_position", 0);
        item.setType_object(type);
        item.setType_position(type_position);

        int pk = preferences.getInt("pk", 0);
        int km = preferences.getInt("km", 0);
        int m = preferences.getInt("m", 0);
        int mPlus = preferences.getInt("mPlus", 0);

        if (type_position == 0) {
            item.setKm(km);
            if (calc) {
                m += mPlus;
                if (m > 99) {
                    String mm = String.valueOf(m);
                    String[] list = mm.split("");
                    StringBuilder builder = new StringBuilder();
                    builder.append(list[2]);
                    builder.append(list[3]);
                    Log.e("tr", "m " + m + " m> " + builder.toString());
                    m = Integer.parseInt(builder.toString());

                    item.setM(builder.toString());
                    pk += 1;

                    if (pk > 9) {
                        pk = 0;
                    }
                } else {
                    item.setM(String.valueOf(m));
                }
            } else {
                item.setM(String.valueOf(m));
            }

            item.setPk(pk);

            preferences.edit().putInt("km", km).apply();
            preferences.edit().putInt("pk", pk).apply();
            preferences.edit().putInt("m", m).apply();
        }

        int editY = preferences.getInt("editY", EDIT_Y);
        int editZ = preferences.getInt("editZ", EDIT_Z);

        int ca = (int) (aMeasObj.m_dBleDistance * 1000);
        item.setY((int) (ca * Math.sin(Math.toRadians(aMeasObj.m_dBleAngle))) + editY);
        item.setZ((int) (ca * Math.cos(Math.toRadians(aMeasObj.m_dBleAngle))) + editZ);
        item.setAngle(aMeasObj.m_dBleAngle);
        item.setDistance(ca);

        item.setDate(System.currentTimeMillis());

        checkTolerance(item);
        realm.commitTransaction();

        preferences.edit().remove("type_select").apply();
        preferences.edit().remove("type_position").apply();
    }

    private boolean checkTolerance(Measure measure) {
        if (measure.getType_position() == 0 | measure.getType_position() == 1) {
            int editZ01From = preferences.getInt("editZ01From", TYPE_Z_0_1_FROM);
            int editZ01Before = preferences.getInt("editZ01Before", TYPE_Z_0_1_BEFORE);
            measure.setToleranceZ(measure.getZ() > editZ01From && measure.getZ() < editZ01Before);
            int editY01From = preferences.getInt("editY01From", TYPE_Y_0_1_FROM);
            int editY01Before = preferences.getInt("editY01Before", TYPE_Y_0_1_BEFORE);
            measure.setToleranceY(measure.getY() > editY01From && measure.getY() < editY01Before);
        } else if (measure.getType_position() == 2) {
            int editZ2From = preferences.getInt("editZ2From", TYPE_Z_2_FROM);
            measure.setToleranceZ(measure.getZ() > editZ2From);
            measure.setToleranceY(true);
        } else if (measure.getType_position() == 3 | measure.getType_position() == 4) {
            int editZ34From = preferences.getInt("editZ34From", TYPE_Z_3_4_FROM);
            measure.setToleranceZ(measure.getZ() > editZ34From);
            measure.setToleranceY(true);
        } else {
            int editZ5From = preferences.getInt("editZ5From", TYPE_Z_5_FROM);
            measure.setToleranceZ(measure.getZ() > editZ5From);
            measure.setToleranceY(true);
        }

        return false;
    }

    private String[] numbers = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."};

    public String getData(String val) {
        StringBuilder builder = new StringBuilder();
        String[] list = val.split("");
        for (String text : list) {

            for (String number : numbers) {
                if (number.equals(text)) {
                    builder.append(text);
                    break;
                }
            }
        }

        return builder.toString();
    }

    public boolean isBluetoothEnabled() {
        return this.bluetoothAdapter.isEnabled();
    }

    public void enableBluetooth() {
        this.bluetoothAdapter.enable();
    }

    public void connectToDevice(BleDevice device) {
        this.device = device;

        connectedDevice = device.device.connectGatt(this, true, callback);

        Log.e("tr", "connect " + connectedDevice.toString());
    }

    @Override
    public void onDestroy() {
        //disconnect();
        super.onDestroy();
    }

    public void disconnect() {
        if (connectedDevice != null) {
            connectedDevice.close();
            connectedDevice.disconnect();
            //this.connectionStateChanged.onDeviceDisconnected();
            mNotificationManager.cancelAll();
            connectedDevice = null;
            device = null;
        }
    }

    public void setConnectionStateChanged(OnConnectionStateChanged connectionStateChanged) {
        this.connectionStateChanged = connectionStateChanged;
    }

    public boolean isConnected() {
        return this.connectedDevice != null;
    }

    public String getDeviecName() {
        if (this.connectedDevice == null) {
            return null;
        }
        return this.connectedDevice.getDevice().getName();
    }
}