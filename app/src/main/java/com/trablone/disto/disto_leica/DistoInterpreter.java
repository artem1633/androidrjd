package com.trablone.disto.disto_leica;

import android.content.SharedPreferences;
import android.content.res.Resources;

import com.trablone.disto.R;

public class DistoInterpreter {
    public static final int CmdDistoAngle = 4;
    public static final int CmdDistoDist = 2;
    public static final int CmdDistoDistAngle = 3;
    public static final int CmdDistoGetSWVersion = 5;
    public static final int CmdDistoGetSerNbr = 6;
    public static final int CmdDistoLaserOff = 8;
    public static final int CmdDistoLaserOn = 7;
    public static final int CmdDistoOff = 1;
    public static final int CmdDistoOn = 0;
    public static final int CmdNoConfirm = 10;
    public static final int CmdRecCfm = 9;
    private static final boolean f101D = false;
    public static final int Disto5 = 0;
    public static final int Disto510 = 4;
    public static final int Disto810 = 5;
    public static final int DistoA6 = 1;
    public static final int DistoD3aBT = 3;
    public static final int DistoD8 = 2;
    private static final int DistoMsgAccuracy = 51;
    private static final int DistoMsgAddConstant = 58;
    private static final int DistoMsgArea = 314;
    private static final int DistoMsgCode1 = 71;
    private static final int DistoMsgCode2 = 72;
    private static final int DistoMsgCode3 = 73;
    private static final int DistoMsgDeviceNumber = 12;
    private static final int DistoMsgDistance = 31;
    private static final int DistoMsgHorizontalDistance = 32;
    private static final int DistoMsgKeycode = 5000;
    private static final int DistoMsgPointNumber = 11;
    private static final int DistoMsgSignalTest = 53;
    private static final int DistoMsgTypeAndSWVersion = 13;
    private static final int DistoMsgUserinput = 30;
    private static final int DistoMsgVerticalAngle = 22;
    private static final int DistoMsgVerticalDistance = 33;
    private static final int DistoMsgVolume = 315;
    private static final String TAG = "DISTOtransfer";
    private static final String TAGKeycode = "DistoKeyCode";
    private static final int WI11 = 11;
    private static final int WI12 = 12;
    private static final int WI13 = 13;
    private static final int WI22 = 22;
    private static final int WI30 = 30;
    private static final int WI31 = 31;
    private static final int WI314 = 314;
    private static final int WI315 = 315;
    private static final int WI32 = 32;
    private static final int WI33 = 33;
    private static final int WI5000 = 5000;
    private static final int WI51 = 51;
    private static final int WI53 = 53;
    private static final int WI58 = 58;
    private static final int WI71 = 71;
    private static final int WI72 = 72;
    private static final int WI73 = 73;
    static final int kNoResult = 0;
    static final int kResultAngle = 2;
    static final int kResultDistance = 1;
    static final int kResultDistoVersion = 5;
    static final int kResultError = 3;
    static final int kResultKeyCode = 4;
    private static final String[][] sDistoCommandTable;
    public static final int u_1_1000ft = 4;
    public static final int u_1_10mm = 5;
    public static final int u_Angle_180 = 14;
    public static final int u_Angle_360 = 15;
    public static final int u_Angle_90 = 13;
    public static final int u_Angle_Percent = 16;
    public static final int u_Angle_in_ft = 18;
    public static final int u_Angle_mm_m = 17;
    public static final int u_None = 0;
    public static final int u_cm = 11;
    public static final int u_ft_in_1_16in = 7;
    public static final int u_ft_in_1_32in = 8;
    public static final int u_ft_in_1_8in = 6;
    public static final int u_in_1_10 = 9;
    public static final int u_in_1_16 = 19;
    public static final int u_in_1_32 = 10;
    public static final int u_m = 3;
    public static final int u_mV = 1;
    public static final int u_m_cm_BT3a = 20;
    public static final int u_mm = 2;
    public static final int u_yd = 12;
    public final int FeetInchSpaceInchFract = 1;
    public final int FeetInchTabInchFract = 3;
    public final int FeetTabInchNoFract = 2;
    int mDistoDeviceNbr = 0;
    public int mDistoType = 2;
    int m_ErrorCode = 0;
    public int m_ImperialMode = 1;
    public int m_MetricConvert = 0;
    String m_ResultString;
    String m_ResultUnitString;
    int m_SendConfirmation = 0;
    public boolean m_bIntegerMode = false;
    boolean m_bResultMetric = false;
    public boolean m_bResultUseEnter = false;
    public boolean m_bResultUseTab = false;
    public boolean m_bResultUseUnit = false;
    int m_eResultType = 0;
    int m_nMetricResultDezimals = 0;

    private class GsiItemAccess {
        static final int MinGsiLenght = 8;
        String m_GsiItem;

        GsiItemAccess(String GsiItem) {
            this.m_GsiItem = GsiItem;
        }

        boolean isAck() {
            return this.m_GsiItem.startsWith("?");
        }

        boolean isErrorCode() {
            return this.m_GsiItem.startsWith("@");
        }

        int getErrorCode() {
            try {
                return (int) Double.parseDouble(this.m_GsiItem.substring(2));
            } catch (NumberFormatException e) {
                return 0;
            }
        }

        public boolean isString() {
            return this.m_GsiItem.startsWith("!");
        }

        public String getString() {
            return this.m_GsiItem.substring(1);
        }

        boolean isValidWI() {
            return this.m_GsiItem.length() >= 8;
        }

        int getWI() {
            int wi = 0;
            if (this.m_GsiItem.length() >= 8) {
                try {
                    wi = Integer.parseInt(this.m_GsiItem.substring(0, 4).replace(".", ""));
                } catch (NumberFormatException e) {
                }
            }
            return wi;
        }

        int getRawUnit() {
            int unit = 0;
            if (this.m_GsiItem.length() >= 8) {
                try {
                    unit = Integer.parseInt(this.m_GsiItem.substring(5, 6));
                } catch (NumberFormatException e) {
                }
            }
            return unit;
        }

        String getDez1() {
            String ret = "";
            if (this.m_GsiItem.length() >= 8) {
                return this.m_GsiItem.substring(6).replace("\n", "").replace("\r", "").replace(" ", "");
            }
            return ret;
        }

        String getDez2() {
            String ret = "";
            if (this.m_GsiItem.length() >= 8) {
                return this.m_GsiItem.substring(11).replace("\n", "").replace("\r", "").replace(" ", "");
            }
            return ret;
        }
    }

    static {
        sDistoCommandTable = new String[11][];
        sDistoCommandTable[0] = new String[]{"a", "a", "a", "a", null, null};
        sDistoCommandTable[1] = new String[]{"b", "b", "b", "b", null, null};
        sDistoCommandTable[2] = new String[]{"g", "g", "g", "g", "g", "g"};
        sDistoCommandTable[3] = new String[]{null, null, "gi", "gi", "g", "g"};
        sDistoCommandTable[4] = new String[]{null, null, "iv", "iv", null, null};
        sDistoCommandTable[5] = new String[]{"N00N", "N00N", "N00N", "N00N", null, null};
        sDistoCommandTable[6] = new String[]{"N02N", "N02N", "N02N", "N02N", null, null};
        sDistoCommandTable[7] = new String[]{"o", "o", "o", "o", "o", "o"};
        sDistoCommandTable[8] = new String[]{"p", "p", "p", "p", "p", "p"};
        sDistoCommandTable[9] = new String[]{null, "cfm", "cfm", "cfm", null, null};
        sDistoCommandTable[10] = new String[]{null, null, "cfb 0", "cfb 0", null, null};

    }

    DistoInterpreter() {
    }

    public void UpdateSettings(SharedPreferences prefs, Resources res) {
        this.m_bResultUseTab = prefs.getBoolean(res.getString(R.string.kDistoTab), false);
        this.m_bResultUseEnter = prefs.getBoolean(res.getString(R.string.kDistoEnter), true);
        this.m_bResultUseUnit = prefs.getBoolean(res.getString(R.string.kDistoUnits), false);
        switch ((int) Double.parseDouble(prefs.getString(res.getString(R.string.kDistoMetric), "0"))) {
            case 0:
                this.m_MetricConvert = 0;
                this.m_bIntegerMode = false;
                break;
            case 1:
                this.m_MetricConvert = 2;
                this.m_bIntegerMode = false;
                break;
            case 2:
                this.m_MetricConvert = 11;
                this.m_bIntegerMode = false;
                break;
            case 3:
                this.m_MetricConvert = 3;
                this.m_bIntegerMode = false;
                break;
            case 4:
                this.m_MetricConvert = 2;
                this.m_bIntegerMode = true;
                break;
        }
        switch ((int) Double.parseDouble(prefs.getString(res.getString(R.string.kDistoImperial), "0"))) {
            case 0:
                this.m_ImperialMode = 1;
                return;
            case 1:
                this.m_ImperialMode = 2;
                return;
            case 2:
                this.m_ImperialMode = 3;
                return;
            default:
                return;
        }
    }

    private boolean isMetric(int eUnit) {
        if (5 == eUnit || 2 == eUnit || 3 == eUnit || 11 == eUnit) {
            return true;
        }
        return false;
    }

    boolean hasDistoCmd(int cmd) {
        if (sDistoCommandTable[cmd][this.mDistoType] != null) {
            return true;
        }
        return false;
    }

    String getDistoCmd(int cmd) {
        return sDistoCommandTable[cmd][this.mDistoType];
    }

    private int getUnit(int rawGsiUnit) {
        switch (rawGsiUnit) {
            case 0:
                return 3;
            case 1:
                return 4;
            case 2:
                return 9;
            case 3:
                return 10;
            case 4:
                if (1 == this.mDistoType || 2 == this.mDistoType || 3 == this.mDistoType) {
                    return 2;
                }
                return 3;
            case 5:
                if (3 == this.mDistoType) {
                    return 20;
                }
                return 11;
            case 6:
                return 5;
            case 7:
                return 12;
            case 8:
                return 7;
            case 9:
                return 8;
            default:
                return 0;
        }
    }

    private int getAngleUnit(int rawGsiUnit) {
        switch (rawGsiUnit) {
            case 0:
                return 13;
            case 1:
                return 14;
            case 2:
                return 15;
            case 3:
                return 16;
            case 4:
                return 17;
            case 5:
                return 18;
            default:
                return 0;
        }
    }

    private String FormatImperial(int ft, int inch, int inx, int inu) {
        String ret = new String();
        switch (this.m_ImperialMode) {
            case 1:
                return String.format("%d.%02d %d/%d", new Object[]{Integer.valueOf(ft), Integer.valueOf(inch), Integer.valueOf(inx), Integer.valueOf(inu)});
            case 2:
                return String.format("%d\t%02d", new Object[]{Integer.valueOf(ft), Integer.valueOf(inch)});
            case 3:
                return String.format("%d.%02d\t%d/%d", new Object[]{Integer.valueOf(ft), Integer.valueOf(inch), Integer.valueOf(inx), Integer.valueOf(inu)});
            default:
                return ret;
        }
    }

    private String FormatImperial(int inch, int inx, int inu) {
        String ret = String.format("%d %d/%d", new Object[]{Integer.valueOf(inch), Integer.valueOf(inx), Integer.valueOf(inu)});
        switch (this.m_ImperialMode) {
            case 1:
                return String.format("%d %d/%d", new Object[]{Integer.valueOf(inch), Integer.valueOf(inx), Integer.valueOf(inu)});
            case 2:
                return String.format("%d\t", new Object[]{Integer.valueOf(inch)});
            case 3:
                return String.format("%d\t%d/%d", new Object[]{Integer.valueOf(inch), Integer.valueOf(inx), Integer.valueOf(inu)});
            default:
                return ret;
        }
    }

    int AnalyzeAndInterpretGsiItem(String gsiItem) {
        GsiItemAccess aItem = new GsiItemAccess(gsiItem);
        this.m_SendConfirmation = 0;
        this.m_eResultType = 0;
        this.m_ErrorCode = 0;
        if (aItem.isAck()) {
            return 0;
        }
        if (aItem.isErrorCode()) {
            this.m_ErrorCode = aItem.getErrorCode();
            this.m_eResultType = 3;
            return 1;
        } else if (!aItem.isValidWI()) {
            return 0;
        } else {
            int wi = aItem.getWI();
            int eUnit;
            switch (wi) {
                case 11:
                    this.m_ResultString = aItem.getDez1();
                    this.m_ResultUnitString = "";
                    this.m_SendConfirmation++;
                    break;
                case 12:
                    this.mDistoDeviceNbr = Integer.getInteger(aItem.getDez1()).intValue();
                    this.m_SendConfirmation++;
                    break;
                case 13:
                    int Type = (int) Double.parseDouble(aItem.getDez1().substring(0, 5));
                    if (Type == 73) {
                        this.mDistoType = 0;
                    } else if (Type == 74) {
                        this.mDistoType = 1;
                    } else if (Type == 75) {
                        this.mDistoType = 2;
                    } else if (Type == 76) {
                        this.mDistoType = 3;
                    }
                    this.m_eResultType = 5;
                    break;
                case 22:
                    this.m_eResultType = 2;
                    this.m_SendConfirmation++;
                    eUnit = getAngleUnit(aItem.getRawUnit());
                    double dAngle = Double.parseDouble(aItem.getDez1());
                    switch (eUnit) {
                        case 13:
                        case 14:
                        case 15:
                            this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dAngle / 100.0d)});
                            this.m_ResultUnitString = "°";
                            this.m_nMetricResultDezimals = 2;
                            break;
                        case 16:
                            this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dAngle / 100.0d)});
                            this.m_ResultUnitString = "%";
                            this.m_nMetricResultDezimals = 2;
                            break;
                        case 17:
                            this.m_ResultString = String.format("%.01f", new Object[]{Double.valueOf(dAngle / 10.0d)});
                            this.m_ResultUnitString = "mm/m";
                            this.m_nMetricResultDezimals = 1;
                            break;
                        case 18:
                            this.m_ResultString = String.format("%.01f", new Object[]{Double.valueOf(dAngle / 10.0d)});
                            this.m_ResultUnitString = "in/ft";
                            this.m_nMetricResultDezimals = 1;
                            break;
                        default:
                            break;
                    }
                case 30:
                    this.m_ResultString = aItem.getDez1();
                    this.m_SendConfirmation++;
                    break;
                case 31:
                case 32:
                case 33:
                case 314:
                case 315:
                    eUnit = getUnit(aItem.getRawUnit());
                    this.m_eResultType = 1;
                    this.m_SendConfirmation++;
                    String Measurement = aItem.getDez1();
                    int iUnitPower = 1;
                    if (wi == 314) {
                        iUnitPower = 2;
                    }
                    if (wi == 315) {
                        iUnitPower = 3;
                    }
                    double dDistance = Double.parseDouble(Measurement);
                    int iDistance = (int) dDistance;
                    int iMeasure;
                    int in;
                    int ft;
                    int inu;
                    int in32;
                    switch (eUnit) {
                        case 0:
                            this.m_ResultString = String.format("%.03f", new Object[]{Double.valueOf(dDistance / 1000.0d)});
                            this.m_ResultUnitString = "";
                            this.m_nMetricResultDezimals = 3;
                            break;
                        case 2:
                            this.m_ResultUnitString = "mm";
                            this.m_bResultMetric = true;
                            switch (this.m_MetricConvert) {
                                case 3:
                                    this.m_ResultString = String.format("%.04f", new Object[]{Double.valueOf(dDistance / 1000.0d)});
                                    this.m_nMetricResultDezimals = 4;
                                    this.m_ResultUnitString = "m";
                                    break;
                                case 11:
                                    this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dDistance / 10.0d)});
                                    this.m_nMetricResultDezimals = 2;
                                    this.m_ResultUnitString = "cm";
                                    break;
                                default:
                                    this.m_ResultString = String.format("%.0f", new Object[]{Double.valueOf(dDistance)});
                                    this.m_nMetricResultDezimals = 1;
                                    break;
                            }
                            switch (iUnitPower) {
                                case 2:
                                    this.m_ResultUnitString += "²";
                                    break;
                                case 3:
                                    this.m_ResultUnitString += "³";
                                    break;
                            }
                            if (this.m_bIntegerMode) {
                                this.m_nMetricResultDezimals = 0;
                                break;
                            }
                            break;
                        case 3:
                            dDistance /= 1000.0d;
                            this.m_ResultUnitString = "m";
                            this.m_bResultMetric = true;
                            switch (this.m_MetricConvert) {
                                case 2:
                                    this.m_ResultString = String.format("%.01f", new Object[]{Double.valueOf(dDistance * 1000.0d)});
                                    this.m_ResultUnitString = "mm";
                                    this.m_nMetricResultDezimals = 1;
                                    break;
                                case 11:
                                    this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dDistance * 100.0d)});
                                    this.m_ResultUnitString = "cm";
                                    this.m_nMetricResultDezimals = 2;
                                    break;
                                default:
                                    this.m_ResultString = String.format("%.03f", new Object[]{Double.valueOf(dDistance)});
                                    this.m_nMetricResultDezimals = 3;
                                    break;
                            }
                            if (this.m_bIntegerMode) {
                                this.m_nMetricResultDezimals = 0;
                            }
                            switch (iUnitPower) {
                                case 2:
                                    this.m_ResultUnitString += "²";
                                    break;
                                case 3:
                                    this.m_ResultUnitString += "³";
                                    break;
                                default:
                                    break;
                            }
                        case 4:
                            this.m_ResultUnitString = "ft";
                            this.m_bResultMetric = true;
                            this.m_nMetricResultDezimals = 2;
                            if (2 == this.mDistoType || 3 == this.mDistoType) {
                                this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dDistance / 100.0d)});
                            } else if (iUnitPower == 3) {
                                this.m_ResultString = String.format("%.01f", new Object[]{Double.valueOf(dDistance / 10.0d)});
                            } else {
                                this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dDistance / 100.0d)});
                            }
                            switch (iUnitPower) {
                                case 2:
                                    this.m_ResultUnitString += "²";
                                    break;
                                case 3:
                                    this.m_ResultUnitString += "³";
                                    break;
                                default:
                                    break;
                            }
                        case 5:
                            dDistance /= 10000.0d;
                            this.m_ResultUnitString = "m";
                            this.m_bResultMetric = true;
                            switch (this.m_MetricConvert) {
                                case 2:
                                    this.m_ResultString = String.format("%.01f", new Object[]{Double.valueOf(dDistance * 1000.0d)});
                                    this.m_nMetricResultDezimals = 1;
                                    this.m_ResultUnitString = "mm";
                                    break;
                                case 11:
                                    this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dDistance * 100.0d)});
                                    this.m_nMetricResultDezimals = 2;
                                    this.m_ResultUnitString = "cm";
                                    break;
                                default:
                                    this.m_ResultString = String.format("%.04f", new Object[]{Double.valueOf(dDistance)});
                                    this.m_nMetricResultDezimals = 4;
                                    break;
                            }
                            switch (iUnitPower) {
                                case 2:
                                    this.m_ResultUnitString += "²";
                                    break;
                                case 3:
                                    this.m_ResultUnitString += "³";
                                    break;
                            }
                            if (this.m_bIntegerMode) {
                                this.m_nMetricResultDezimals = 0;
                                break;
                            }
                            break;
                        case 6:
                            iMeasure = iDistance;
                            int in8 = iMeasure % 100;
                            in = ((iMeasure % 10000) - in8) / 100;
                            ft = ((iMeasure - (in * 100)) - in8) / 10000;
                            inu = 16;
                            if (in8 % 8 == 0) {
                                in8 /= 8;
                                inu = 2;
                            } else if (in8 % 4 == 0) {
                                in8 /= 4;
                                inu = 4;
                            } else if (in8 % 2 == 0) {
                                in8 /= 2;
                                inu = 8;
                            }
                            this.m_ResultString = FormatImperial(ft, in, in8, inu);
                            this.m_ResultUnitString = "";
                            break;
                        case 7:
                            iMeasure = iDistance;
                            int in16 = iMeasure % 100;
                            in = ((iMeasure % 10000) - in16) / 100;
                            ft = ((iMeasure - (in * 100)) - in16) / 10000;
                            inu = 16;
                            if (in16 % 8 == 0) {
                                in16 /= 8;
                                inu = 2;
                            } else if (in16 % 4 == 0) {
                                in16 /= 4;
                                inu = 4;
                            } else if (in16 % 2 == 0) {
                                in16 /= 2;
                                inu = 8;
                            }
                            this.m_ResultString = FormatImperial(ft, in, in16, inu);
                            this.m_ResultUnitString = "";
                            this.m_bResultMetric = false;
                            break;
                        case 8:
                            iMeasure = iDistance;
                            in32 = iMeasure % 100;
                            in = ((iMeasure % 10000) - in32) / 100;
                            ft = ((iMeasure - (in * 100)) - in32) / 10000;
                            inu = 32;
                            if (in32 % 16 == 0) {
                                in32 /= 16;
                                inu = 2;
                            } else if (in32 % 8 == 0) {
                                in32 /= 8;
                                inu = 4;
                            } else if (in32 % 4 == 0) {
                                in32 /= 4;
                                inu = 8;
                            } else if (in32 % 2 == 0) {
                                in32 /= 2;
                                inu = 16;
                            }
                            this.m_ResultString = FormatImperial(ft, in, in32, inu);
                            this.m_ResultUnitString = "";
                            this.m_bResultMetric = false;
                            break;
                        case 9:
                            this.m_bResultMetric = true;
                            this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dDistance / 10.0d)});
                            this.m_ResultUnitString = "in";
                            this.m_nMetricResultDezimals = 2;
                            switch (iUnitPower) {
                                case 2:
                                    this.m_ResultUnitString += "²";
                                    break;
                                case 3:
                                    this.m_ResultUnitString += "³";
                                    break;
                                default:
                                    break;
                            }
                        case 10:
                            in32 = iDistance % 100;
                            in = (iDistance - in32) / 100;
                            inu = 32;
                            if (in32 % 16 == 0) {
                                in32 /= 16;
                                inu = 2;
                            } else if (in32 % 8 == 0) {
                                in32 /= 8;
                                inu = 4;
                            } else if (in32 % 4 == 0) {
                                in32 /= 4;
                                inu = 8;
                            } else if (in32 % 2 == 0) {
                                in32 /= 2;
                                inu = 16;
                            }
                            this.m_ResultString = FormatImperial(in, in32, inu);
                            this.m_ResultUnitString = "";
                            this.m_bResultMetric = false;
                            break;
                        case 11:
                            dDistance /= 100.0d;
                            this.m_ResultUnitString = "cm";
                            this.m_bResultMetric = true;
                            switch (this.m_MetricConvert) {
                                case 2:
                                    this.m_ResultString = String.format("%.01f", new Object[]{Double.valueOf(dDistance * 1000.0d)});
                                    this.m_nMetricResultDezimals = 1;
                                    this.m_ResultUnitString = "mm";
                                    break;
                                case 11:
                                    this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dDistance * 100.0d)});
                                    this.m_nMetricResultDezimals = 2;
                                    this.m_ResultUnitString = "cm";
                                    break;
                                default:
                                    this.m_ResultString = String.format("%.2f", new Object[]{Double.valueOf(dDistance)});
                                    this.m_nMetricResultDezimals = 2;
                                    break;
                            }
                            switch (iUnitPower) {
                                case 2:
                                    this.m_ResultUnitString += "²";
                                    break;
                                case 3:
                                    this.m_ResultUnitString += "³";
                                    break;
                            }
                            if (this.m_bIntegerMode) {
                                this.m_nMetricResultDezimals = 0;
                                break;
                            }
                            break;
                        case 12:
                            this.m_bResultMetric = true;
                            this.m_ResultString = String.format("%.03f", new Object[]{Double.valueOf(dDistance / 1000.0d)});
                            this.m_ResultUnitString = "yd";
                            this.m_nMetricResultDezimals = 3;
                            switch (iUnitPower) {
                                case 2:
                                    this.m_ResultUnitString += "²";
                                    break;
                                case 3:
                                    this.m_ResultUnitString += "³";
                                    break;
                                default:
                                    break;
                            }
                        case 19:
                            in32 = iDistance % 100;
                            in = (iDistance - in32) / 100;
                            inu = 16;
                            if (in32 % 8 == 0) {
                                in32 /= 8;
                                inu = 2;
                            } else if (in32 % 4 == 0) {
                                in32 /= 4;
                                inu = 4;
                            } else if (in32 % 2 == 0) {
                                in32 /= 2;
                                inu = 8;
                            }
                            this.m_ResultString = FormatImperial(in, in32, inu);
                            this.m_ResultUnitString = "";
                            this.m_bResultMetric = false;
                            break;
                        case 20:
                            dDistance /= 100.0d;
                            this.m_ResultUnitString = "m";
                            this.m_bResultMetric = true;
                            switch (this.m_MetricConvert) {
                                case 2:
                                    this.m_ResultString = String.format("%.01f", new Object[]{Double.valueOf(dDistance * 1000.0d)});
                                    this.m_nMetricResultDezimals = 1;
                                    this.m_ResultUnitString = "mm";
                                    break;
                                case 11:
                                    this.m_ResultString = String.format("%.02f", new Object[]{Double.valueOf(dDistance * 100.0d)});
                                    this.m_nMetricResultDezimals = 2;
                                    this.m_ResultUnitString = "cm";
                                    break;
                                default:
                                    this.m_ResultString = String.format("%.2f", new Object[]{Double.valueOf(dDistance)});
                                    this.m_nMetricResultDezimals = 2;
                                    break;
                            }
                            switch (iUnitPower) {
                                case 2:
                                    this.m_ResultUnitString += "²";
                                    break;
                                case 3:
                                    this.m_ResultUnitString += "³";
                                    break;
                            }
                            if (this.m_bIntegerMode) {
                                this.m_nMetricResultDezimals = 0;
                                break;
                            }
                            break;
                        default:
                            break;
                    }
                case 51:
                    this.m_SendConfirmation++;
                    this.m_ResultString = aItem.getDez1();
                    break;
                case 53:
                    this.m_SendConfirmation++;
                    this.m_ResultString = aItem.getDez1();
                    break;
                case 71:
                case 72:
                case 73:
                    this.m_SendConfirmation++;
                    this.m_ResultString = aItem.getDez1();
                    this.m_ResultString.trim();
                    break;
                case 5000:
                    this.m_SendConfirmation++;
                    this.m_eResultType = 4;
                    this.m_ResultString = aItem.getDez2();
                    break;
            }
            return 1;
        }
    }
}
