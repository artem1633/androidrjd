package com.trablone.disto.disto_leica;

public class MeterEvent {
    private String result;

    public MeterEvent(String result) {
        this.result = result.trim();
    }

    public String getResult() {
        return this.result;
    }
}
