package com.trablone.disto.disto_leica;

import android.content.Context;
import android.widget.GridView;

public class ExcelGridView extends GridView {
    public static ExcelPosition m_Position = new ExcelPosition();
    public String[] m_ValArray;
    public Boolean[] m_bFormulaArray;

    public ExcelGridView(Context context) {
        super(context);
    }

    public String numToColumn(int nNbr) {
        boolean bNotReady = true;
        String result = "";
        while (bNotReady) {
            result = Character.toString((char) ('A' + (nNbr % 26))) + result;
            if (nNbr < 26) {
                bNotReady = false;
            }
            nNbr = (nNbr / 26) - 1;
        }
        return result;
    }

    void clearExcelValuesArray() {
        if (this.m_ValArray != null) {
            for (int k = 0; k < this.m_ValArray.length; k++) {
                this.m_ValArray[k] = null;
                this.m_bFormulaArray[k] = Boolean.valueOf(false);
            }
        }
    }
}
