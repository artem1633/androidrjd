package com.trablone.disto.disto_leica;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;

public class BleDevice {
    private String deviceName;
    public BluetoothDevice device;

    public BleDevice(BluetoothDevice device) {
        this.deviceName = device.getName();
        this.device = device;
    }

    public BleDevice(String name) {
        this.deviceName = name;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
