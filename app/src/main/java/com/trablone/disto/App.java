package com.trablone.disto;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.trablone.disto.disto_leica.BleDevice;
import com.trablone.disto.disto_leica.BluetoothService;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends Application {

    @Nullable
    private BluetoothService bluetoothService;
    private ServiceConnection connection = new C03881();

    public static App getInstance() {
        return INSTANCE;
    }

    private static App INSTANCE;

    class C03881 implements ServiceConnection {
        C03881() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            App.this.bluetoothService = ((BluetoothService.BleBinder) iBinder).getService();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            App.this.bluetoothService = null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(configuration);

        YandexMetricaConfig config = YandexMetricaConfig.newConfigBuilder("3e450a08-66f6-4e96-bf95-378f94be1f08").build();
        YandexMetrica.activate(getApplicationContext(), config);
        YandexMetrica.enableActivityAutoTracking(this);
    }

    public void bind(){
        if (bluetoothService == null){
            startService(new Intent(this, BluetoothService.class));
            bindService(new Intent(this, BluetoothService.class), this.connection, BIND_AUTO_CREATE);
        }
    }

    public void unbind(){
        bluetoothService.disconnect();
        unbindService(connection);

    }

    @Nullable
    public BluetoothService getBluetoothService() {
        return this.bluetoothService;
    }

    public BleDevice getDevice(){
        if (getBluetoothService() != null){
            return getBluetoothService().getDevice();
        }

        return null;
    }

}
