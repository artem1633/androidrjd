package com.trablone.disto;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.trablone.disto.adapters.DeviceAdapter;
import com.trablone.disto.disto_leica.BleDevice;
import com.trablone.disto.disto_leica.BluetoothService;
import com.trablone.disto.disto_leica.MeterEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DeviceActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private BluetoothAdapter bluetoothAdapter;
    private Map<BleDevice, BluetoothDevice> devices = new HashMap();
    private BluetoothService service;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);
        ButterKnife.bind(this);
        service = ((App) getApplication()).getBluetoothService();
        if (service != null && service.isBluetoothEnabled()) {

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            this.devices.clear();
            if (this.bluetoothAdapter.isDiscovering()) {
                this.bluetoothAdapter.cancelDiscovery();
            }

            final BluetoothLeScanner scanner = this.bluetoothAdapter.getBluetoothLeScanner();

            final ScanCallback[] scanCallback = new ScanCallback[]{null};

            final DeviceAdapter adapter = new DeviceAdapter(this, new DeviceAdapter.ItemClickListener() {
                @Override
                public void itemClick(BleDevice item) {
                    connect(item);
                    scanner.stopScan(scanCallback[0]);
                    finish();
                }
            });

            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);

            scanCallback[0] = new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    super.onScanResult(callbackType, result);
                    if (callbackType == 1) {
                        BleDevice device = new BleDevice(result.getDevice());
                        if (!DeviceActivity.this.devices.containsValue(result.getDevice())) {
                            DeviceActivity.this.devices.put(device, result.getDevice());
                            adapter.addDevice(new BleDevice(result.getDevice()));
                        }
                    }
                }
            };

            scanner.startScan(scanCallback[0]);
        } else {
            Toast.makeText(DeviceActivity.this, "Включите Bluetooth", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    public void connect(BleDevice device) {

        try {
            service.connectToDevice(device);
        } catch (Exception e2) {
            e2.printStackTrace();
        }

    }


}
