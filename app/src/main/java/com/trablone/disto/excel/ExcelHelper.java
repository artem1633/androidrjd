package com.trablone.disto.excel;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.trablone.disto.models.Measure;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ExcelHelper {

    private Context context;
    private WritableSheet writableSheet;

    public ExcelHelper(Context context) {
        this.context = context;
    }

    public boolean writeTable(List<Measure> list, String name, int mPlus) {
        String fileName = new SimpleDateFormat("dd-MMMM-yyyy HH-mm").format(System.currentTimeMillis());
        WritableWorkbook writableWorkbook = createWorkbook(fileName + "_" + name + ".xls");
        writableSheet = createSheet(writableWorkbook, "sheetName", 0);

        int row = 0;

        try {

            String[] names = name.split("_");

            writeCell(0, row, names[0], false, writableSheet, Alignment.LEFT);
            writeCell(5, row, "Путь " + names[1], false, writableSheet, Alignment.LEFT);

            row += 1;
            writeCell(0, row, "Дата измерения: " + fileName, false, writableSheet, Alignment.LEFT);
            writeCell(5, row, "Шаг замеров: " + mPlus, false, writableSheet, Alignment.LEFT);

            row += 1;
            writeCell(0, row, "№ п/п", false, writableSheet, Alignment.LEFT);
            writeCell(1, row, "КМ", false, writableSheet, Alignment.LEFT);
            writeCell(2, row, "ПК", false, writableSheet, Alignment.LEFT);

            writeCell(3, row, "Гор", false, writableSheet, Alignment.LEFT);
            writeCell(4, row, "Верт", false, writableSheet, Alignment.LEFT);

            writeCell(5, row, "Обьект", false, writableSheet, Alignment.LEFT);

            row += 1;

            for (int i = 0; i < list.size(); i++) {
                Measure item = list.get(i);
                row += 1;

                writeCell(0, row, String.valueOf(i), false, writableSheet, Alignment.RIGHT);
                writeCell(1, row, String.valueOf(item.getKm()), false, writableSheet, Alignment.RIGHT);
                writeCell(2, row, item.getPk() + "+" + item.getM(), false, writableSheet, Alignment.LEFT);
                writeCell(3, row, item.getSummZ(), false, writableSheet, Alignment.CENTRE);
                writeCell(4, row, item.getSummY(), false, writableSheet, Alignment.CENTRE);
                writeCell(5, row, item.getType_object(), false, writableSheet, Alignment.LEFT);
            }
        }catch (WriteException e){
            e.printStackTrace();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }


        try {
            writableWorkbook.write();
            writableWorkbook.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (WriteException e) {
            e.printStackTrace();
            return false;
        }


        Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();

        return true;
    }

    private void writeCell(int columnPosition, int rowPosition, String contents, boolean headerCell, WritableSheet sheet, Alignment alignment) throws RowsExceededException, WriteException {
        //create a new cell with contents at position
        Label newCell = new Label(columnPosition, rowPosition, contents);

        if (headerCell) {
            //give header cells size 10 Arial bolded
            WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
            WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
            //center align the cells' contents

            headerFormat.setAlignment(alignment);
            newCell.setCellFormat(headerFormat);
        }

        sheet.addCell(newCell);

    }

    private WritableWorkbook createWorkbook(String fileName) {
        //exports must use a temp file while writing to avoid memory hogging
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setUseTemporaryFileDuringWrite(true);

        //get the sdcard's directory
        File sdCard = Environment.getExternalStorageDirectory();
        //add on the your app's path
        File dir = new File(sdCard.getAbsolutePath() + "/Disto");
        //make them in case they're not there
        dir.mkdirs();
        //create a standard java.io.File object for the Workbook to use
        File wbfile = new File(dir, fileName);

        WritableWorkbook wb = null;

        try {
            //create a new WritableWorkbook using the java.io.File and
            //WorkbookSettings from above
            wb = Workbook.createWorkbook(wbfile, wbSettings);
        } catch (IOException ex) {
            Log.e("tr", ex.getStackTrace().toString());
            Log.e("tr", ex.getMessage());
        }

        return wb;
    }

    private WritableSheet createSheet(WritableWorkbook wb, String sheetName, int sheetIndex) {
        //create a new WritableSheet and return it
        return wb.createSheet(sheetName, sheetIndex);
    }
}
