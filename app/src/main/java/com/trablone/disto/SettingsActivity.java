package com.trablone.disto;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

import com.trablone.disto.models.Measure;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.edit_z)
    EditText editZ;
    @BindView(R.id.edit_y)
    EditText editY;
    @BindView(R.id.edit_z_0_1_from)
    EditText editZ01From;
    @BindView(R.id.edit_z_0_1_before)
    EditText editZ01Before;
    @BindView(R.id.edit_y_0_1_from)
    EditText editY01From;
    @BindView(R.id.edit_y_0_1_before)
    EditText editY01Before;
    @BindView(R.id.edit_z_2_from)
    EditText editZ2From;
    @BindView(R.id.edit_z_3_4_from)
    EditText editZ34From;
    @BindView(R.id.edit_z_5_from)
    EditText editZ5From;

    private SharedPreferences preferences;

    public static final int EDIT_Y = 225;
    public static final int EDIT_Z = 760;
    public static final int TYPE_Z_0_1_FROM = 1720;
    public static final int TYPE_Z_0_1_BEFORE = 1750;
    public static final int TYPE_Y_0_1_FROM = 150;
    public static final int TYPE_Y_0_1_BEFORE = 220;
    public static final int TYPE_Z_2_FROM = 3100;
    public static final int TYPE_Z_3_4_FROM = 2750;
    public static final int TYPE_Z_5_FROM = 3100;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        editY.setText(String.valueOf(preferences.getInt("editY", EDIT_Y)));
        editZ.setText(String.valueOf(preferences.getInt("editZ", EDIT_Z)));

        editZ01From.setText(String.valueOf(preferences.getInt("editZ01From", TYPE_Z_0_1_FROM)));
        editZ01Before.setText(String.valueOf(preferences.getInt("editZ01Before", TYPE_Z_0_1_BEFORE)));
        editY01From.setText(String.valueOf(preferences.getInt("editY01From", TYPE_Y_0_1_FROM)));
        editY01Before.setText(String.valueOf(preferences.getInt("editY01Before", TYPE_Y_0_1_BEFORE)));

        editZ2From.setText(String.valueOf(preferences.getInt("editZ2From", TYPE_Z_2_FROM)));
        editZ34From.setText(String.valueOf(preferences.getInt("editZ34From", TYPE_Z_3_4_FROM)));
        editZ5From.setText(String.valueOf(preferences.getInt("editZ5From", TYPE_Z_5_FROM)));

        editZ.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = getNumber(editZ);
                preferences.edit().putInt("editZ", i).apply();
            }
        });

        editY.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = getNumber(editY);
                preferences.edit().putInt("editY", i).apply();
            }
        });

        editY01From.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = getNumber(editY01From);
                preferences.edit().putInt("editY01From", i).apply();
            }
        });

        editY01Before.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = getNumber(editY01Before);
                preferences.edit().putInt("editY01Before", i).apply();
            }
        });

        editZ01From.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = getNumber(editZ01From);
                preferences.edit().putInt("editZ01From", i).apply();
            }
        });

        editZ01Before.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = getNumber(editZ01Before);
                preferences.edit().putInt("editZ01Before", i).apply();
            }
        });

        editZ2From.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = getNumber(editZ2From);
                preferences.edit().putInt("editZ2From", i).apply();
            }
        });

        editZ34From.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = getNumber(editZ34From);
                preferences.edit().putInt("editZ34From", i).apply();
            }
        });

        editZ5From.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = getNumber(editZ5From);
                preferences.edit().putInt("editZ5From", i).apply();
            }
        });

    }

    private int getNumber(EditText editText){
        String text = editText.getText().toString();
        if (TextUtils.isEmpty(text)){
            return 0;
        }

        return Integer.parseInt(text);
    }



}
