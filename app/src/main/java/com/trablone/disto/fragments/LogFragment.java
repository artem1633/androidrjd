package com.trablone.disto.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.trablone.disto.EditActivity;
import com.trablone.disto.R;
import com.trablone.disto.adapters.LogAdapter;
import com.trablone.disto.models.Measure;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

public class LogFragment extends BaseListFragment {

    private LogAdapter adapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new LogAdapter(getActivity(), new LogAdapter.ItemClickListener() {
            @Override
            public void itemClick(Measure item) {
                Intent intent = new Intent(getActivity(), EditActivity.class);
                intent.putExtra("date", item.getDate());
                startActivity(intent);
            }

            @Override
            public void itemLongClick(final Measure item) {
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.app_name)
                        .setMessage("Удалить " + item.getSummY() + " " + item.getSummZ() + " ?")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                realm.beginTransaction();
                                item.deleteFromRealm();
                                realm.commitTransaction();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();

            }
        });

        recyclerView.setAdapter(adapter);
        realm.addChangeListener(new RealmChangeListener<Realm>() {
            @Override
            public void onChange(Realm realm) {
                whereList();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.add(0, 1, 0, "Сохранить").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 1:
                String name = preferences.getString("file_name", null);
                int mPlus= preferences.getInt("mPlus", 0);
                if (exelHelper.writeTable(adapter.getList(),name, mPlus)) {
                    preferences.edit().remove("file_name").apply();
                    realm.beginTransaction();
                    realm.delete(Measure.class);
                    realm.commitTransaction();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        whereList();
    }

    private void whereList() {
        RealmResults<Measure> list = realm.where(Measure.class).sort("date", Sort.DESCENDING).findAll();
        adapter.setList(list);
    }
}
