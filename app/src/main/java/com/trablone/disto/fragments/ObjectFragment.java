package com.trablone.disto.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.trablone.disto.R;
import com.trablone.disto.adapters.ObjectAdapter;
import com.trablone.disto.models.TypeObject;

import java.util.ArrayList;
import java.util.List;

public class ObjectFragment extends BaseListFragment {

    private ObjectAdapter adapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        int select = preferences.getInt("type_position", 0);

        adapter = new ObjectAdapter(getActivity(), new ObjectAdapter.ItemClickListener() {
            @Override
            public void itemClick(TypeObject item, int position) {
                if (position < 5){
                    preferences.edit().putString("type_select", item.getTitle()).apply();
                    preferences.edit().putInt("type_position", position).apply();
                    adapter.setSelect(position);
                }else {
                    showOtherDialog();
                }

            }
        });

        adapter.setSelect(select);

        recyclerView.setAdapter(adapter);

    }

    private void showOtherDialog(){
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_other, null, false);
        final EditText editText = view.findViewById(R.id.item_edit);
        editText.setText(preferences.getString("edit_other", ""));

        new AlertDialog.Builder(getContext())
                .setView(view)
                .setTitle("Прочее")
                .setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        preferences.edit().putString("edit_other", editText.getText().toString()).apply();
                        preferences.edit().putString("type_select", editText.getText().toString()).apply();
                        preferences.edit().putInt("type_position", 5).apply();
                        adapter.setSelect(5);
                    }
                })
                .show();
    }
}
