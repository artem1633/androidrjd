package com.trablone.disto.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trablone.disto.R;
import com.trablone.disto.excel.ExcelHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class BaseListFragment extends Fragment {


    public Realm realm;
    public ExcelHelper exelHelper;


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    public SharedPreferences preferences;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        realm = Realm.getDefaultInstance();
        setHasOptionsMenu(true);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        exelHelper = new ExcelHelper(getActivity());

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

}
