package com.trablone.disto.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.trablone.disto.App;
import com.trablone.disto.EditActivity;
import com.trablone.disto.MainActivity;
import com.trablone.disto.R;
import com.trablone.disto.adapters.MeasureAdapter;
import com.trablone.disto.adapters.ObjectAdapter;
import com.trablone.disto.disto_leica.BluetoothService;
import com.trablone.disto.models.Measure;
import com.trablone.disto.models.TypeObject;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class MeasureFragment extends BaseListFragment {

    private MeasureAdapter adapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new MeasureAdapter(getActivity(), new MeasureAdapter.ItemClickListener() {
            @Override
            public void itemClick(Measure item) {
                Intent intent = new Intent(getActivity(), EditActivity.class);
                intent.putExtra("date", item.getDate());
                startActivity(intent);
            }

            @Override
            public void itemLongClick(final Measure item) {
                String[] list = new String[]{"Измерить повторно", "Изменить тип обьекта", "Изменить КМ и ПК", "Удалить измерение"};
                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, list);

                final BluetoothService service = App.getInstance().getBluetoothService();

                if (service == null)
                    return;
                if (item == null) {
                    service.setMeasureId(null);
                    return;
                }

                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.app_name)
                        .setAdapter(adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (which == 0) {
                                    service.setMeasureId(item.getId());
                                    MeasureFragment.this.adapter.notifyDataSetChanged();
                                } else if (which == 1) {
                                    showObjectDialog(item);
                                } else if (which == 2) {
                                    MainActivity activity = (MainActivity)getActivity();
                                    activity.editMeasure(realm, item);
                                } else if (which == 3) {
                                    realm.beginTransaction();
                                    item.deleteFromRealm();
                                    realm.commitTransaction();
                                }
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();

            }
        });

        recyclerView.setAdapter(adapter);
        realm.addChangeListener(new RealmChangeListener<Realm>() {
            @Override
            public void onChange(Realm realm) {
                whereList();
            }
        });
    }

    private AlertDialog alertDialog;

    private void showObjectDialog(final Measure measure) {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_object, null, false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        ObjectAdapter adapter = new ObjectAdapter(getActivity(), new ObjectAdapter.ItemClickListener() {
            @Override
            public void itemClick(TypeObject item, int position) {
                if (position < 5) {
                    realm.beginTransaction();
                    measure.setType_position(position);
                    measure.setType_object(item.getTitle());
                    realm.commitTransaction();

                } else {
                    showOtherDialog(measure);
                }

                alertDialog.dismiss();
            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.app_name)
                .setView(view)
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }


    private void showOtherDialog(final Measure measure) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_other, null, false);
        final EditText editText = view.findViewById(R.id.item_edit);
        editText.setText(preferences.getString("edit_other", ""));

        new AlertDialog.Builder(getContext())
                .setView(view)
                .setTitle("Прочее")
                .setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        realm.beginTransaction();
                        measure.setType_position(5);
                        measure.setType_object(editText.getText().toString());
                        realm.commitTransaction();
                    }
                })
                .show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.add(0, 1, 0, "Сохранить").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menu.add(0, 2, 0, "Очистить журнал").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                String name = preferences.getString("file_name", null);
                if (!TextUtils.isEmpty(name)){
                    int mPlus= preferences.getInt("mPlus", 0);
                    if (exelHelper.writeTable(adapter.getList(), name, mPlus)) {
                        realm.beginTransaction();
                        realm.delete(Measure.class);
                        realm.commitTransaction();
                    }
                }else {
                    Toast.makeText(getActivity(), "Имя файла отсутствует", Toast.LENGTH_SHORT).show();
                }

                return true;
            case 2:
                realm.beginTransaction();
                realm.delete(Measure.class);
                realm.commitTransaction();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        whereList();
    }

    private void whereList() {
        RealmResults<Measure> list = realm.where(Measure.class).sort("date").findAll();
        Log.e("tr", "count " + list.size());
        adapter.setList(list);

        recyclerView.scrollToPosition(list.size() - 1);
    }


}
