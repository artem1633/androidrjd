package com.trablone.disto.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trablone.disto.App;
import com.trablone.disto.R;
import com.trablone.disto.disto_leica.BluetoothService;
import com.trablone.disto.models.Measure;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeasureAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;

    public void setList(List<Measure> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private int select = -1;

    public List<Measure> getList() {
        return list;
    }

    private List<Measure> list;
    private ItemClickListener listener;

    public MeasureAdapter(Context context, ItemClickListener listener) {
        this.context = context;
        this.list = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_measure, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, final int position) {
        final Measure item = list.get(position);

        final BluetoothService service = App.getInstance().getBluetoothService();

        ViewHolder holder = (ViewHolder) holder1;
        if (position == 0) {
            holder.cardHeader.setVisibility(View.VISIBLE);
        } else {
            holder.cardHeader.setVisibility(View.GONE);
        }

        if (service != null && service.getMeasureId() != null && service.getMeasureId().contains(item.getId())) {
            holder.layoutColor.setBackgroundResource(R.color.colorSelectType);
        } else {
            holder.layoutColor.setBackgroundResource(R.color.colorWhite);
        }

        if (item.isToleranceZ()){
            holder.textViewA.setBackgroundResource(R.color.colorWhite);
        }else {
            holder.textViewA.setBackgroundResource(R.color.colorTolerance);
        }

        if (item.isToleranceY()){
            holder.textViewB.setBackgroundResource(R.color.colorWhite);
        }else {
            holder.textViewB.setBackgroundResource(R.color.colorTolerance);
        }

        int number = position + 1;
        holder.textNumber.setText(String.valueOf(number));
        holder.textType.setText(item.getType_object());

        if (item.getType_position() == 0) {
            holder.textLocation.setText(item.getKm() + "КМ\nПК" + item.getPk() + "+" + item.getM());
        } else {
            holder.textLocation.setText(null);
        }

        holder.textViewA.setText(item.getSummZ());
        holder.textViewB.setText(item.getSummY());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClick(item);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (service != null && service.getMeasureId() != null && service.getMeasureId().contains(item.getId())) {
                    listener.itemLongClick(null);
                } else {
                    listener.itemLongClick(item);
                }

                notifyDataSetChanged();
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_a)
        TextView textViewA;
        @BindView(R.id.item_number)
        TextView textNumber;
        @BindView(R.id.item_type)
        TextView textType;
        @BindView(R.id.item_location)
        TextView textLocation;
        @BindView(R.id.card_header)
        CardView cardHeader;
        @BindView(R.id.item_b)
        TextView textViewB;
        @BindView(R.id.layout_color)
        LinearLayout layoutColor;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ItemClickListener {
        void itemClick(Measure item);

        void itemLongClick(Measure item);
    }

}