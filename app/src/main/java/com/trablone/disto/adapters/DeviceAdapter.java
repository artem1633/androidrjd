package com.trablone.disto.adapters;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trablone.disto.R;
import com.trablone.disto.disto_leica.BleDevice;

import java.util.ArrayList;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by trablone on 18.01.18.
 */

public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.ViewHolder>{

    private Context context;

    public List<BleDevice> getList() {
        return list;
    }

    public void setList(List<BleDevice> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    private List<BleDevice> list;
    private ItemClickListener listener;

    public DeviceAdapter(Context context,  ItemClickListener listener){
        this.context = context;
        this.list = new ArrayList<>();
        this.listener = listener;
    }

    public void addDevice(BleDevice device){
        list.add(device);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_devices, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final BleDevice item = list.get(position);
        holder.textView.setText("name: " + item.getDeviceName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClick(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_title)
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ItemClickListener{
        void itemClick(BleDevice item);
    }
}
