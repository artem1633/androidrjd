package com.trablone.disto.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trablone.disto.R;
import com.trablone.disto.models.Measure;
import com.trablone.disto.models.TypeObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ObjectAdapter extends RecyclerView.Adapter<ObjectAdapter.ViewHolder>{

    private Context context;

    public void setList(List<TypeObject> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public List<TypeObject> getList() {
        return list;
    }

    private List<TypeObject> list;
    private ItemClickListener listener;

    public ObjectAdapter(Context context, ItemClickListener listener){
        this.context = context;

        list = new ArrayList<>();
        list.add(new TypeObject("Борт\nплатформы"));
        list.add(new TypeObject("Место\nвидимого стеснения"));
        list.add(new TypeObject("Теневой\nнавес"));
        list.add(new TypeObject("МАФ"));
        list.add(new TypeObject("Торцевое\nограждение"));
        list.add(new TypeObject("Прочее"));
        this.listener = listener;
    }

    public int getSelect() {
        return select;
    }

    public void setSelect(int select) {
        this.select = select;
        notifyDataSetChanged();
    }

    private int select;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_object, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final TypeObject item = list.get(position);

        holder.texttitle.setText(item.getTitle().replace("\n", " "));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClick(item, position);
            }
        });

        if (select == position){
            holder.texttitle.setBackgroundResource(R.color.colorSelectType);
        }else {
            holder.texttitle.setBackgroundResource(R.color.colorWhite);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_title)
        TextView texttitle;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ItemClickListener{
        void itemClick(TypeObject item, int position);

    }
}
