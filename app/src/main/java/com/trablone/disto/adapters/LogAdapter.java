package com.trablone.disto.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trablone.disto.R;
import com.trablone.disto.models.Measure;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LogAdapter extends RecyclerView.Adapter<LogAdapter.ViewHolder>{

    private Context context;

    public void setList(List<Measure> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public List<Measure> getList() {
        return list;
    }

    private List<Measure> list;
    private ItemClickListener listener;

    public LogAdapter(Context context, LogAdapter.ItemClickListener listener){
        this.context = context;

        this.list = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_log, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Measure item = list.get(position);

        holder.layout.setVisibility(View.VISIBLE);


        holder.textViewA.setText("Длина " + item.getSummZ() + "m");
        holder.textViewB.setText("Высота " + item.getSummY() + "m");

        holder.textViewDate.setText(item.getFormatDate());

        holder.textViewDistance.setText(String.valueOf(item.getDistance()) + "m");
        holder.textViewAngle.setText(String.valueOf(item.getAngle()) + "°");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.itemClick(item);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.itemLongClick(item);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.item_a)
        TextView textViewA;

        @BindView(R.id.item_b)
        TextView textViewB;

        @BindView(R.id.item_distance)
        TextView textViewDistance;

        @BindView(R.id.item_angle)
        TextView textViewAngle;

        @BindView(R.id.item_date)
        TextView textViewDate;

        @BindView(R.id.layout_type_log)
        LinearLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ItemClickListener{
        void itemClick(Measure item);

        void itemLongClick(Measure item);
    }
}

