package com.trablone.disto;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.trablone.disto.disto_leica.BleDevice;
import com.trablone.disto.disto_leica.BleDistoMeasurementFormat;
import com.trablone.disto.disto_leica.BluetoothService;
import com.trablone.disto.fragments.LogFragment;
import com.trablone.disto.fragments.MeasureFragment;
import com.trablone.disto.fragments.ObjectFragment;
import com.trablone.disto.models.Measure;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;


    private final int PERMISSION_KAY = 155;
    private List<String> permissionList;

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() == 0) {
                    Fragment fragment = new MeasureFragment();
                    setFragment(fragment);
                } else if (tab.getPosition() == 1) {
                    Fragment fragment = new LogFragment();
                    setFragment(fragment);
                } else {
                    Fragment fragment = new ObjectFragment();
                    setFragment(fragment);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });

        tabLayout.addTab(tabLayout.newTab().setText("Измерения"), true);
        tabLayout.addTab(tabLayout.newTab().setText("Журнал"));
        tabLayout.addTab(tabLayout.newTab().setText("Объект"));

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        setSubTitle();

        enablePermissions();

        EventBus.getDefault().register(this);

        ((App) getApplication()).bind();

        device = ((App) getApplication()).getDevice();
        if (device != null){
            connectDevice(device);
        }
        BleDistoMeasurementFormat format = new BleDistoMeasurementFormat();
        format.m_dBleAngle = 0.9;
        format.m_dBleDistance = 1456;
        //final Realm realm = Realm.getDefaultInstance();
        //BluetoothService service = ((App) getApplication()).getBluetoothService();
        //service.writeMeasure(realm, format, false);
    }

    private void setFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }

    private boolean enablePermissions() {
        permissionList = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            permissionList.add(Manifest.permission.READ_PHONE_STATE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        int size = permissionList.size();
        String[] permissions = new String[size];
        for (int i = 0; i < permissionList.size(); i++) {
            permissions[i] = permissionList.get(i);
        }

        if (size > 0) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_KAY);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_KAY) {
            boolean READ_PHONE_STATE = false;
            boolean WRITE_EXTERNAL_STORAGE = false;
            boolean ACCESS_FINE_LOCATION = false;
            boolean ACCESS_COARSE_LOCATION = false;

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                ACCESS_COARSE_LOCATION = true;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                ACCESS_FINE_LOCATION = true;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                READ_PHONE_STATE = true;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                WRITE_EXTERNAL_STORAGE = true;

            if (WRITE_EXTERNAL_STORAGE && READ_PHONE_STATE && ACCESS_FINE_LOCATION && ACCESS_COARSE_LOCATION) {
                enablePermissions();
            }
        }
    }

    protected void onStart() {
        super.onStart();

    }

    protected void onStop() {
        super.onStop();

    }

    private BleDevice device;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void connectDevice(BleDevice item) {
        device = item;
        Log.e("tr", "device" + item.getDeviceName());

        getSupportActionBar().setTitle(item.getDeviceName());
        setSubTitle();
        invalidateOptionsMenu();

        final Realm realm = Realm.getDefaultInstance();
        RealmResults<Measure> list = realm.where(Measure.class).sort("date", Sort.DESCENDING).findAll();

        if (list.size() == 0) {
            setParameters(null);
        }
    }

    public void setSubTitle(){
        getSupportActionBar().setSubtitle(preferences.getString("file_name", null));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void metricFromBle(final BleDistoMeasurementFormat aMeasObj) {
        setParameters(aMeasObj);
    }

    private void setParameters(final BleDistoMeasurementFormat aMeasObj){

        if (alertDialog != null && alertDialog.isShowing()){
            alertDialog.dismiss();
        }

        String name = preferences.getString("obj_name", null);
        String number = preferences.getString("obj_number", null);

        showFirstDialog(new FirstShowListener() {
            @Override
            public void onSuccess(String name, String number) {
                preferences.edit().remove("file_name").apply();
                preferences.edit().putString("file_name", name + "_" + number).apply();
                setSubTitle();

                int pk = preferences.getInt("pk", 0);
                int km = preferences.getInt("km", 0);
                int m = preferences.getInt("m", 0);
                showKmPkMDialog(new KmPkMListener() {
                    @Override
                    public void onSuccess() {
                        int m = preferences.getInt("mPlus", 0);
                        showMPlusDialog(new MPlusListener() {
                            @Override
                            public void onSuccess(int m) {
                                if (aMeasObj != null){
                                    final Realm realm = Realm.getDefaultInstance();
                                    BluetoothService service = ((App) getApplication()).getBluetoothService();
                                    service.writeMeasure(realm, aMeasObj, false);
                                }
                            }



                        }, m);
                    }

                    @Override
                    public void onSuccess(int pk, int km, int m) {

                    }
                }, pk, km, m, true);
            }
        }, name, number);
    }

    public void editMeasure(final Realm realm, final Measure measure){

        showKmPkMDialog(new KmPkMListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onSuccess(int pk, int km, int m) {

                realm.beginTransaction();
                measure.setM(String.valueOf(m));
                measure.setPk(pk);
                measure.setKm(km);
                realm.commitTransaction();

                if (measure.getType_position() == 0){
                    preferences.edit().putInt("pk", pk).apply();
                    preferences.edit().putInt("km", km).apply();
                    preferences.edit().putInt("m", m).apply();
                }

            }
        }, measure.getPk(), measure.getKm(), Integer.parseInt(measure.getM()), false);
    }

    private AlertDialog alertDialog;

    private void showKmPkMDialog(final KmPkMListener listener, final int pk, final int km, final int m, final boolean edit){
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_kmpkm, null, false);
        final EditText editKm = view.findViewById(R.id.edit_km);
        final EditText editPk = view.findViewById(R.id.edit_pk);
        final EditText editM = view.findViewById(R.id.edit_m);

        if (pk >= 0){
            editPk.setText(String.valueOf(pk));
        }

        if (km >= 0){
            editKm.setText(String.valueOf(km));
        }

        if (m >= 0){
            editM.setText(String.valueOf(m));
        }

        editPk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int number = getNumber(editPk);
                if (number > 9){
                    editPk.setText(null);
                    editPk.setError("Запрет на ввод значения свыше 9");
                }
            }
        });

        editM.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int number = getNumber(editM);
                if (number > 99){
                    editM.setText(null);
                    editM.setError("Запрет на ввод значения свыше 99");
                }
            }
        });

        alertDialog = new AlertDialog.Builder(this)
                .setView(view)
                .setCancelable(false)
                .setPositiveButton("Принять", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int pk = getNumber(editPk);
                        int km = getNumber(editKm);
                        int m = getNumber(editM);

                        if (pk < 0 | km < 0 | m < 0) {
                           showKmPkMDialog(listener, pk, km, m, edit);
                        } else {
                            if (edit){
                                preferences.edit().putInt("pk", pk).apply();
                                preferences.edit().putInt("km", km).apply();
                                preferences.edit().putInt("m", m).apply();
                            }

                            listener.onSuccess();
                            listener.onSuccess(pk, km, m);
                        }
                    }
                })
                .show();

    }

    private void showMPlusDialog(final MPlusListener listener, int mPlus){
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_m_plus, null, false);
        final EditText editM = view.findViewById(R.id.edit_m_plus);
        if (mPlus >= 0){
            editM.setText(String.valueOf(mPlus));
        }

        alertDialog = new AlertDialog.Builder(this)
                .setView(view)
                .setCancelable(false)
                .setPositiveButton("Принять", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int mPlus = getNumber(editM);
                        preferences.edit().putInt("mPlus", mPlus).apply();
                        listener.onSuccess(mPlus);

                    }
                })
                .show();

    }

    private int getNumber(EditText editText){
        String text = editText.getText().toString();
        if (TextUtils.isEmpty(text)){
            return -1;
        }

        return Integer.parseInt(text);
    }

    private void showFirstDialog(final FirstShowListener listener, String name, String number) {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_new_measures, null, false);
        final EditText editName = view.findViewById(R.id.item_name);
        final EditText editNumber = view.findViewById(R.id.item_number);
        if (name != null) {
            editName.setText(name);
        }
        if (number != null) {
            editNumber.setText(number);
        }

        alertDialog = new AlertDialog.Builder(this)
                .setView(view)
                .setCancelable(false)
                .setPositiveButton("Принять", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = editName.getText().toString();
                        String number = editNumber.getText().toString();
                        if (TextUtils.isEmpty(name)) {
                            showFirstDialog(listener, name, number);
                        } else if (TextUtils.isEmpty(number)) {
                            showFirstDialog(listener, name, number);
                        } else {
                            preferences.edit().putString("obj_name", name).apply();
                            preferences.edit().putString("obj_number", number).apply();
                            listener.onSuccess(name, number);
                        }
                    }
                })
                .show();
    }

    private interface FirstShowListener {
        void onSuccess(String name, String number);
    }

    private interface KmPkMListener {
        void onSuccess();
        void onSuccess(final int pk, final int km, final int m);
    }

    private interface MPlusListener {
        void onSuccess(final int m);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 3, 0, "Устройства").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menu.add(0, 4, 0, "Справка").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menu.add(0, 6, 0, "Параметры").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        menu.removeItem(5);
        menu.removeItem(7);
        if (device != null && device.device != null) {
            menu.add(0, 5, 0, "Отключить").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            menu.add(0, 7, 0, "Задать параметры").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }

        //menu.add(0, 2, 0, "Настройки").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 7:
                setParameters(null);
                return true;
            case 6:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case 3:
                startActivity(new Intent(this, DeviceActivity.class));
                return true;
            case 4:
                startActivity(new Intent(this, InfoActivity.class));
                return true;
            case 5:
                EventBus.getDefault().post(new BleDevice("Отключено"));
                ((App) getApplication()).unbind();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean laser;

    /*@OnClick(R.id.button_laser)
    public void buttonLaserOnOff() {
        if (laser) {
            service.SendCommand(8);
        } else {
            service.SendCommand(7);
        }
        laser = !laser;
    }*/

    //@OnClick(R.id.button_dist)
    //public void buttonDist() {
    //    service.SendCommand(2);
    //}

    //@OnClick(R.id.button_dist_angle)
    //public void buttonDistAndAngle() {
    //    service.SendCommand(3);
    //}


    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }
}
