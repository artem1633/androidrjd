package com.trablone.disto.models;

public class TypeObject {

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TypeObject(String title) {
        this.title = title;
    }

    private String title;
}
