package com.trablone.disto.models;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Measure extends RealmObject {


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @PrimaryKey
    private String id;


    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    private long date;

    public double getY() {
        return y;
    }

    public void setY(int a) {
        this.y = a;
    }

    public double getZ() {
        return z;
    }

    public void setZ(int b) {
        this.z = b;
    }

    private int y;
    private int z;

    public double getPlus_a() {
        return plus_a;
    }

    public void setPlus_a(double plus_a) {
        this.plus_a = plus_a;
    }

    public double getPlus_b() {
        return plus_b;
    }

    public void setPlus_b(double plus_b) {
        this.plus_b = plus_b;
    }

    private double plus_a;
    private double plus_b;

    public String getType_object() {
        return type_object;
    }

    public void setType_object(String type_object) {
        this.type_object = type_object;
    }

    private String type_object;

    public int getType_position() {
        return type_position;
    }

    public void setType_position(int type_position) {
        this.type_position = type_position;
    }

    private int type_position;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public double getAngle() {
        return angle;
    }

    public String getFormatDate(){
        return new SimpleDateFormat("yyyy-mm-dd hh:mm").format(new Date(getDate()));
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    private int distance;
    private double angle;

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public int getPk() {
        return pk;
    }

    public void setPk(int pk) {
        this.pk = pk;
    }



    private int km;
    private int pk;


    public boolean isToleranceY() {
        return toleranceY;
    }

    public void setToleranceY(boolean toleranceY) {
        this.toleranceY = toleranceY;
    }

    public boolean isToleranceZ() {
        return toleranceZ;
    }

    public void setToleranceZ(boolean toleranceZ) {
        this.toleranceZ = toleranceZ;
    }

    private boolean toleranceY;
    private boolean toleranceZ;

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    private String m;

    public String getSummY(){

        //return String.format("%.4f", new Object[]{Double.valueOf(y)});
        return String.valueOf(y);
    }

    public String getSummZ(){

        //return String.format("%.4f", new Object[]{Double.valueOf(z)});
        return String.valueOf(z);
    }
}
