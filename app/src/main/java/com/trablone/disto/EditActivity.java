package com.trablone.disto;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.trablone.disto.models.Measure;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class EditActivity extends AppCompatActivity {


    @BindView(R.id.item_a)
    EditText editTextA;

    @BindView(R.id.item_b)
    EditText editTextB;

    private SharedPreferences preferences;

    private Realm realm;

    private Measure measure;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        realm = Realm.getDefaultInstance();

        measure = realm.where(Measure.class).equalTo("date", getIntent().getLongExtra("date", 0)).findFirst();

        editTextA.setText(measure.getSummY());
        editTextB.setText(measure.getSummZ());
    }

    /*@OnClick(R.id.button_save)
    public void save(){

        double a = Double.parseDouble(editTextA.getText().toString());
        double b = Double.parseDouble(editTextB.getText().toString());

        realm.beginTransaction();
        measure.setY(a);
        measure.setZ(b);
        realm.commitTransaction();

        finish();
    }*/

}
